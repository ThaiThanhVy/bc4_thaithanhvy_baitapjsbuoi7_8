var numArr = [];

// Thêm số
function themSo() {
    var themSo = document.getElementById("txt-them-so").value * 1;
    // console.log('themSo: ', themSo);
    document.getElementById("txt-them-so").value = "";
    numArr.push(themSo);
    document.getElementById("kq-them-so").innerText = `${numArr}`
}
// end Thêm số

// Tổng số dương
function tinhTongSoDuong() {
    var sum = 0;
    var count = 0;
    var sum2 = 0;
    for (var index = 0; index < numArr.length; index++) {
        var currentNum = numArr[index];
        if (currentNum > 0) {
            sum = sum + currentNum;
        }
    }
    document.getElementById("kq-tong-so-duong").innerText = `${sum}`
}
// end Tổng số dương


// Đếm số
function demSo() {
    var count = 0;
    for (var index = 0; index < numArr.length; index++) {
        var currentNum = numArr[index];
        if (currentNum > 0) {
            count++;
        }

    }
    document.getElementById("kq-dem-so").innerText = `${count}`
}
// end Đếm số


// Tìm số nhỏ nhất
function soNhoNhat() {
    var soNhoNhat = numArr[0];
    for (var index = 0; index < numArr.length; index++) {
        var currentNum = numArr[index];
        if (soNhoNhat > currentNum) {
            soNhoNhat = currentNum;
        }
    }
    document.getElementById("so-nho-nhat").innerText = `${soNhoNhat}`
}
// end Tìm số nhỏ nhất

// Tìm số dương nhỏ nhất
function soDuongNhoNhat() {
    var soDuongNhoNhat = numArr[0];
    for (var index = 0; index < numArr.length; index++) {
        var currentNum = numArr[index];
        if (soDuongNhoNhat > currentNum && currentNum > 0) {
            soDuongNhoNhat = currentNum;
        }
    }
    document.getElementById("so-duong-nho-nhat").innerText = `${soDuongNhoNhat}`
}
// end Tìm số dương nhỏ nhất


// Tìm số chẵn cuối cùng


function soChanCuoiCung() {
    var result = 0;
    for (index = 0; index < numArr.length; index++) {
        var currentNum = numArr[index];
        if (currentNum % 2 == 0) {
            result = currentNum;
        }
        // console.log('result: ', result);
        document.getElementById("kq-so-chan-cuoi").innerHTML = `${result}`

    }
}


// end Tìm số chẵn cuối cùng







// chuyển đổi vị trí
function chuyenDoi(index1, index2) {
    var array = [];
    array[0] = index2;
    array[1] = index1;
    return array
}

function doiCho() {
    var inputIndex1 = document.getElementById("inputIndex1").value * 1;
    var inputIndex2 = document.getElementById("inputIndex2").value * 1;
    var array = chuyenDoi(inputIndex1, inputIndex2);
    document.getElementById("kq-doi-cho").innerHTML = `Mảng sau khi đổi: ${array}`;
}
//end chuyển đổi vị trí

// Sắp xếp tăng dần
function sapXep() {
    var newArr = numArr.sort(function (item1, item2) {
        if (item1 > item2) {
            return 1;
        } if (item1 < item2) {
            return -1;
        } if (item1 == item2) {
            return 0;
        }
    });
    document.getElementById("sap-xep-tang-dan").innerText = `${newArr}`
}
// end Sắp xếp tăng dần




// Tìm số nguyên tố đầu tiên
function kiemTraSoNguyenTo(n) {
    if (n < 2) {
        return !1;
    }
    for (var i = 2; i <= Math.sqrt(n); i++) {
        if (n % i == 0) {
            return !1;
        }
    }
    return !0;
}

function soNguyen() {
    var n = -1;
    for (i = 0; i < numArr.length; i++) {
        var currentNum = numArr[i];
        if (kiemTraSoNguyenTo(currentNum)) {
            n = currentNum;
            break;
        }
    }
    document.getElementById("so-nguyen-to").innerHTML = `${n}`;
}
// end Tìm số nguyên tố đầu tiên




// Đếm số nguyên
function demSoNguyen() {
    var kq = 0;
    for (i = 0; i < numArr.length; i++) {
        var currentNum = numArr[i];
        Number.isInteger(currentNum) && kq++;
    }
    document.getElementById("kq-dem-so-nguyen").innerHTML = `${kq}`;
}

// end Đếm số nguyên

// So sánh lượng số âm và dương
function soSanh() {
    var soDuong = 0;
    var kq = '';
    for (var index = 0; index < numArr.length; index++) {
        var currentNum = numArr[index];
        if (currentNum > 0) {
            soDuong++;
        }

    }
    var soAm = 0
    for (var index = 0; index < numArr.length; index++) {
        var currentMin = numArr[index];
        if (currentMin < 0) {
            soAm++;
        }

    }

    if (soDuong == soAm) {
        kq = 'số dương = số âm';
    } else if (soDuong < soAm) {
        kq = 'số dương < số âm'
    } else {
        kq = 'số dương > sô âm'
    }
    document.getElementById("kq-so-sanh").innerText = `${kq}`
}
// end So sánh lượng số âm và dương